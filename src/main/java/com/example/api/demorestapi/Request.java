package com.example.api.demorestapi;

import lombok.Data;

@Data
public class Request {
    private String faxID;
    private String status;
    private String reason;
    private String systemID;
    private String referenceID;
    private String to;
    private String completedAt;
}
