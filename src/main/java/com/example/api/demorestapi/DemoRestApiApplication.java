package com.example.api.demorestapi;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;

@SpringBootApplication
@RestController
@Slf4j
@EnableCaching
public class DemoRestApiApplication {
    @Autowired
 private Environment env;
    public static void main(String[] args) {
        SpringApplication.run(DemoRestApiApplication.class, args);
    }


    @GetMapping("/ustil/test-api")
	public String test(){
    	log.info("Getting current time");
    	return LocalDateTime.now().toString();
	}


    @PostMapping("v1/umfaxresponse")
    public String umfaxresponse(@RequestBody Request request){
        log.info("Getting current time");
        return "Thanks "+LocalDateTime.now().toString();
    }


	@PostConstruct
    public void init(){
        log.info("aaa = {}",env.getProperty("spring.cloud.gcp.logging.enabled"));
    }
}
